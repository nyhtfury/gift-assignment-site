export function getGiftGivers(lines: string): string[] {
    /**
     * Split a string by new lines, tabs, and commas. Automatically remove blank entries and return the list of gift givers.
     * 
     * @param lines - a string with one or more separators (\n, \r, \t, or ',')
     * @returns a string array split by the separators above.
     */
    return lines.split('\n|\r|\t|,').filter( s => s.length > 0);
}

export function generateGiftReceivers(giftGivers: string[]): [string[], number] {
    /**
     * Given a string array of gift givers, generate a string array of gift receivers of equal length in such an order as to prevent self and circular (pair) gift giving.
     * 
     * @remarks
     * See the circularGiftGiversPresent function for more details.
     * 
     * @param giftGivers - a string array of gift givers
     * @returns a tuple containing 1. a string array of gift receivers, devoid of self-gifters (match index) and circular gifting (paired indices), and 2. the number of shuffles it took to generate said list.
     */
    switch (giftGivers.length) {
        case 0:
            console.log("No gift givers. Must not be in the mood for gifting.")
            return [[], 0];
        case 1:
            console.log("There's only one person giving & receiving gifts... I think you can figure that out.")
            return [giftGivers, 0];
        case 2:
            console.log("With only two people, no iterations were required.")
            return [giftGivers.reverse(), 0];
    }

    var giftReceivers: string[];
    var iterations: number;
    
    do {
        giftReceivers = shuffle(giftGivers);
        iterations++;
    } while (circularGiftGiversPresent(giftGivers, giftReceivers))

    return [giftReceivers, iterations];
}

function shuffle<T>(array: T[]): T[] {
    /**
     * Shuffle the given array using the Fisher-Yates shuffle.
     * 
     * @remarks
     * Copied from https://javascript.info/task/shuffle. It does not modify the given array.
     * 
     * @param array - array of type T
     * @returns array of type T, but in a different order.
     */

    var newArray: T[] = array;

    for (let i = newArray.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
  
      // swap elements array[i] and array[j]
      // we use "destructuring assignment" syntax to achieve that
      // you'll find more details about that syntax in later chapters
      // same can be written as:
      // let t = array[i]; array[i] = array[j]; array[j] = t
      [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
    }
    return newArray;
}

function circularGiftGiversPresent(giftGivers: string[], giftReceivers: string[]): boolean {
    /**
     * Given two lists, see if any of their indices match or if two people are giving each other gifts.
     * Raise an error if lists are different lengths.
     * 
     * @remarks
     * This function is intended for lists greater than length 3. Lists of length 0, 1, or 2 will always return true.
     * 
     * @param giftGivers - string list of gift givers, must be same length as giftReceivers
     * @param giftReceivers - string list of gift receivers, must be same length as giftGivers
     * @returns true if there are self-gifters, or circular gifting (two people giving each other gifts)
     */
    if (giftGivers.length !== giftReceivers.length) {
        throw new Error("List length mismatch in CircularGiftGiversPresent.");
    } else if (giftGivers.length <= 2) {
        return true;
    }

    for (var i:number = 0; i <= giftGivers.length; i++) {
        if (giftGivers[i] === giftReceivers[giftGivers.indexOf(giftReceivers[i])]) {
            return true;
        }
    }

    return false;
}
